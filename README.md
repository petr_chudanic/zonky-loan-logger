zonky-loan-logger
==========================

#What is it?

My attempt at tackling perpetual loading and logging of new loans on Zonky marketplace.

# How to run it?

To run the application issue following command.
	
	./gradlew bootRun

To build and run the application issue the next one.

    ./gradlew build && java -jar build/libs/zonky-loan-logger-1.0.0.jar
    
Requires at least JAVA_HOME with java version 1.8.

# Description

After starting application will periodically check Zonky API for new loans. Those are 
logged into logs/loans.log file.

Application logic entry point can be found in chudanic.petr.zonky.scheduling.LoanLoadScheduler.

All loan loggers should be an implementation of chudanic.petr.zonky.logging.LoanLogWriter class. 
You can register your own implementation in chudanic.petr.zonky.logging.LoggingConfig class.

# Configuration
Application provides several configurable properties within application.properties file.

* `app.version` to define application version
* `app.repository-url` to define repository url
* `app.logging-interval-in-millis` to set interval between consecutive loan loads
* `app.log-output-location` to define location of the log file where loans will be written to
* `api-client.base-url` to point to API's base URL
* `api-client.loans-endpoint` to define an endpoint for querying loans
* `api-client.user-agent-template` to define template for User-Agent request header
* `api-client.page-size` to define X-Page-Size header used for pagination

## Configuration override examples 

To override properties while running built version pass them as JVM options.
 
	./gradlew build && java -jar -Dapp.logging-interval-in-millis=600000 -Dapp.log-output-location=/mylogs/loans.log build/libs/zonky-loan-logger-1.0.0.jar

To override them with bootRun specify them as follows

	./gradlew bootRun -Pargs=--app.log-output-location=/mylogs/loans.log,--app.logging-interval-in-millis=600000