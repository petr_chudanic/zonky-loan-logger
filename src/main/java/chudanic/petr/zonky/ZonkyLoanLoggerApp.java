package chudanic.petr.zonky;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Application for periodical loan retrieval and logging.
 */
@EnableScheduling
@SpringBootApplication
public class ZonkyLoanLoggerApp {

	public static void main(String[] args) {
		SpringApplication.run(ZonkyLoanLoggerApp.class, args);
	}
}
