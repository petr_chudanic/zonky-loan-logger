package chudanic.petr.zonky.apiclient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * Configuration for beans related to API client.
 */
@Configuration
public class ApiClientConfig {

	/**
	 * Provide {@link WebClient} configured with base URL of Zonky API.
	 *
	 * @param baseUrl Zonky API's base URL
	 * @return configured web client
	 */
	@Bean
	public WebClient webClient(@Value("${api-client.base-url}") String baseUrl) {
		return WebClient.builder()
				.baseUrl(baseUrl)
				.build();
	}
}
