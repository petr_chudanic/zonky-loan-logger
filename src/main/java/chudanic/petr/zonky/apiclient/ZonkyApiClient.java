package chudanic.petr.zonky.apiclient;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import chudanic.petr.zonky.apiclient.model.LoanDTO;
import chudanic.petr.zonky.apiclient.model.ZonkyLoansApiResponseDTO;
import chudanic.petr.zonky.model.Loan;
import org.modelmapper.ModelMapper;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * Zonky API client.
 */
@Component
public class ZonkyApiClient {

	private final WebClient client;

	private String appVersion;

	private String appRepositoryUrl;

	private int pageSize;

	private String loansEndpoint;

	private String userAgentTemplate;

	private ModelMapper modelMapper;

	public ZonkyApiClient(@Autowired WebClient client,
			@Autowired ModelMapper modelMapper,
			@Value("${app.version}") String appVersion,
			@Value("${app.repository-url}") String appRepositoryUrl,
			@Value("${api-client.page-size}") int pageSize,
			@Value("${api-client.loans-endpoint}") String loansEndpoint,
			@Value("${api-client.user-agent-template}") String userAgentTemplate) {
		this.client = client;
		this.pageSize = pageSize;
		this.appVersion = appVersion;
		this.appRepositoryUrl = appRepositoryUrl;
		this.loansEndpoint = loansEndpoint;
		this.userAgentTemplate = userAgentTemplate;
		this.modelMapper = modelMapper;
	}

	private Mono<ZonkyLoansApiResponseDTO> getLoansFromZonkyApi(final int page, LocalDateTime start, LocalDateTime end) {
		return client.get().uri(
				builder -> builder.path(loansEndpoint)
						// Filter-out only fields that will be logged
						.queryParam("fields", "id,datePublished,name,amount")
						.queryParam("datePublished__gt", start.format(DateTimeFormatter.ISO_DATE_TIME))
						.queryParam("datePublished__lte", end.format(DateTimeFormatter.ISO_DATE_TIME))
						.build()
		).accept(MediaType.APPLICATION_JSON_UTF8)
				.header("X-Size", Integer.toString(pageSize))
				.header("X-Order", "datePublished")
				.header("X-Page", Integer.toString(page))
				.header("User-Agent", String.format(userAgentTemplate, appVersion, appRepositoryUrl))
				.exchange()
				.flatMap(this::mapLoansResponse);
	}

	private Mono<ZonkyLoansApiResponseDTO> mapLoansResponse(ClientResponse response) {
		if (response.statusCode() != HttpStatus.OK) {
			return Mono.error(new Exception(String.format("There was an logError during loan retrieval.\n" +
					"Status code: [%s]", response.statusCode())));
		}
		Flux<LoanDTO> loans = response.bodyToFlux(LoanDTO.class);
		int totalSize = response.headers().header("X-Total").stream().mapToInt(Integer::valueOf).findFirst()
				.orElseThrow(() -> new RuntimeException("Missing X-Total header value."));
		return Mono.just(new ZonkyLoansApiResponseDTO(loans, totalSize));
	}

	/**
	 * Retrieve all loans between specified dates. Internally pagination is used when response becomes longer
	 * than defined by api-client.page-size property.
	 *
	 * @param start lower limit on loan published date
	 * @param end upper limit on loan published date
	 * @return reactive stream of loans
	 */
	public Flux<Loan> loadLoans(LocalDateTime start, LocalDateTime end) {
		return loadLoans(0, start, end).map(this::convertToEntity);
	}

	private Flux<LoanDTO> loadLoans(final int page, LocalDateTime start, LocalDateTime end) {
		return this.getLoansFromZonkyApi(page, start, end)
				.flatMapMany((response) -> handleResponse(response, page, start, end));
	}

	private Flux<LoanDTO> handleResponse(ZonkyLoansApiResponseDTO response, int page, LocalDateTime start, LocalDateTime end) {
		if (!(response.getTotalSize() <= (page + 1) * pageSize)) {
			return response.getLoans().concatWith(this.loadLoans(page + 1, start, end));
		}
		return response.getLoans();
	}

	private Loan convertToEntity(LoanDTO loanDTO) {
		return modelMapper.map(loanDTO, Loan.class);
	}
}
