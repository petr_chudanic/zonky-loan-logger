package chudanic.petr.zonky.apiclient.model;

import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Loan DTO for working with Zonky API that captures content of selected fields.
 */
public class LoanDTO {

	private Number id;

	private String name;

	private Number amount;

	private ZonedDateTime datePublished;

	@JsonCreator
	public LoanDTO(@JsonProperty("id") Number id,
			@JsonProperty("name") String name,
			@JsonProperty("amount") Number amount,
			@JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
			@JsonProperty("datePublished") ZonedDateTime datePublished) {
		this.id = id;
		this.name = name;
		this.amount = amount;
		this.datePublished = datePublished;
	}

	public Number getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Number getAmount() {
		return amount;
	}

	public ZonedDateTime getDatePublished() {
		return datePublished;
	}
}
