package chudanic.petr.zonky.apiclient.model;

import chudanic.petr.zonky.model.Loan;
import org.modelmapper.ModelMapper;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration that provides mapper for mapping of DTOs to model entities.
 */
@Configuration
public class MapperConfig {

	/**
	 * Provide implementation of {@link ModelMapper} for DTO - model entities conversion.
	 *
	 * @return model mapper
	 */
	@Bean
	public ModelMapper modelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.createTypeMap(LoanDTO.class, Loan.class).setProvider(request -> {
			LoanDTO dto = (LoanDTO) request.getSource();
			return new Loan(dto.getId(), dto.getName(), dto.getAmount(), dto.getDatePublished());
		});

		return modelMapper;
	}
}
