package chudanic.petr.zonky.apiclient.model;

import reactor.core.publisher.Flux;

/**
 * Convenience wrapper for result of call to loans endpoint.
 */
public class ZonkyLoansApiResponseDTO {

	private Flux<LoanDTO> loans;

	private int totalSize;

	public ZonkyLoansApiResponseDTO(Flux<LoanDTO> loans,
			int totalSize) {
		this.loans = loans;
		this.totalSize = totalSize;
	}

	public Flux<LoanDTO> getLoans() {
		return loans;
	}

	public int getTotalSize() {
		return totalSize;
	}
}
