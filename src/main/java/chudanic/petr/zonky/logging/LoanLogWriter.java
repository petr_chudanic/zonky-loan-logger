package chudanic.petr.zonky.logging;

import chudanic.petr.zonky.model.Loan;

/**
 * Interface that specifies methods for loggers used to track loans and errors during their load.
 * Concrete implementation should be provided in {@link LoggingConfig}.
 *
 */
public interface LoanLogWriter {

	/**
	 * Writes loan information into specific location, based on implementation.
	 *
	 * @param loan specific loan that should be logged
	 */
	void logLoan(Loan loan);

	/**
	 * Logs errors that occurred during loans retrieval into separate location.
	 *
	 * @param error specific error to log
	 */
	void logError(Throwable error);
}
