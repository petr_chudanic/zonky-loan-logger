package chudanic.petr.zonky.logging;

import chudanic.petr.zonky.logging.impl.DefaultLoanLogWriter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Logging configuration.
 */
@Configuration
public class LoggingConfig {

	/**
	 * Provide implementation of {@link LoanLogWriter} that will handle logging of loans retrieved from API.
	 *
	 * @return loans' logger implementation
	 */
	@Bean
	public LoanLogWriter loanLogger() {
		return new DefaultLoanLogWriter();
	}
}
