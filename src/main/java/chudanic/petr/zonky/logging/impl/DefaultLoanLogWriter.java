package chudanic.petr.zonky.logging.impl;

import chudanic.petr.zonky.logging.LoanLogWriter;
import chudanic.petr.zonky.model.Loan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation of {@link LoanLogWriter} which logs error to console and loans into the file.
 */
public class DefaultLoanLogWriter implements LoanLogWriter {

	private Logger loansLogger = LoggerFactory.getLogger("loans.logger");

	private Logger errorLogger = LoggerFactory.getLogger("error.logger");

	/**
	 * Logs error to file specified by app.log-output-location property.
	 *
	 * @param loan specific loan that should be logged
	 */
	@Override
	public void logLoan(Loan loan) {
		loansLogger.info(loan.toString());
	}

	/**
	 * Logs error to console.
	 *
	 * @param error specific error to log
	 */
	@Override
	public void logError(Throwable error) {
		errorLogger.error(error.getMessage(), error);
	}
}