package chudanic.petr.zonky.model;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Captures several selected properties of loan from Zonky's marketplace.
 */
public class Loan {

	private Number id;

	private String name;

	private Number amount;

	private ZonedDateTime datePublished;

	public Loan(Number id, String name, Number amount, ZonedDateTime datePublished) {
		this.id = id;
		this.name = name;
		this.amount = amount;
		this.datePublished = datePublished;
	}

	@Override
	public String toString() {
		return String.format("{\nid: %s,\n" +
				"datePublished: %s,\n" +
				"name: %s,\n" +
				"amount: %s\n}", id, datePublished.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME), name, amount);
	}
}
