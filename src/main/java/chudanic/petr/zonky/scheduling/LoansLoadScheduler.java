package chudanic.petr.zonky.scheduling;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import chudanic.petr.zonky.service.LoanLoggingService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Component that is responsible for initiation and sustaining of loans retrieval.
 */
@Component
public class LoansLoadScheduler {

	private LocalDateTime lastLoanLoad;

	private LoanLoggingService loggingService;

	public LoansLoadScheduler(@Value("${app.logging-interval-in-millis}") int loggingIntervalInMillis, @Autowired LoanLoggingService loggingService) {
		this.loggingService = loggingService;
		this.lastLoanLoad = LocalDateTime.now().minus(loggingIntervalInMillis, ChronoUnit.MILLIS);
	}

	/**
	 * Schedules recurring load of loans from Zonky API.
	 */
	@Scheduled(fixedRateString = "${app.logging-interval-in-millis}")
	public void scheduledLoansLoad() {
		LocalDateTime start = lastLoanLoad;
		LocalDateTime end = LocalDateTime.now();
		lastLoanLoad = end;
		loggingService.loadAndLogLoans(start, end);
	}

	public LocalDateTime getLastLoanLoad() {
		return lastLoanLoad;
	}
}
