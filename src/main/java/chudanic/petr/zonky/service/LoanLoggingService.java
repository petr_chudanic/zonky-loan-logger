package chudanic.petr.zonky.service;

import java.time.LocalDateTime;

import chudanic.petr.zonky.apiclient.ZonkyApiClient;
import chudanic.petr.zonky.logging.LoanLogWriter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Main service class that uses API client to retrieve loans and pass them on to logging class.
 */
@Service
public class LoanLoggingService {

	private ZonkyApiClient client;

	private LoanLogWriter loanLogWriter;

	public LoanLoggingService(@Autowired ZonkyApiClient client,
			@Autowired LoanLogWriter loanLogger) {
		this.client = client;
		this.loanLogWriter = loanLogger;
	}

	/**
	 * Retrieve loans published within specified time-frame. Then logs them along with any errors that were encountered.
	 *
	 * @param start lower limit on loan published date
	 * @param end upper limit on loan published date
	 */
	public void loadAndLogLoans(LocalDateTime start, LocalDateTime end) {
		client.loadLoans(start, end).subscribe(loanLogWriter::logLoan, loanLogWriter::logError);
	}
}
