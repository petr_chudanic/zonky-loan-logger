package chudanic.petr.zonky.apiclient;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Stream;

import chudanic.petr.zonky.apiclient.model.LoanDTO;
import chudanic.petr.zonky.model.Loan;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;

public class ZonkyApiClientTest {

	final WebClient webClientMock = Mockito.mock(WebClient.class);

	final WebClient.RequestHeadersUriSpec uriSpecMock = Mockito.mock(WebClient.RequestHeadersUriSpec.class);

	final WebClient.RequestHeadersSpec headersSpecMock = Mockito.mock(WebClient.RequestHeadersSpec.class);

	final ClientResponse clientResponseMock = Mockito.mock(ClientResponse.class);

	final ClientResponse.Headers responseHeadersMock = Mockito.mock(ClientResponse.Headers.class);

	final ModelMapper modelMapper = Mockito.mock(ModelMapper.class);

	final ZonkyApiClient apiClient = new ZonkyApiClient(webClientMock,
			modelMapper,
			"1.0.0",
			"appRepositoryUrl",
			10,
			"loansEndpoint",
			"userAgentTemplate");

	final LocalDateTime startTime = LocalDateTime.MIN;

	final LocalDateTime endTime = LocalDateTime.MAX;

	@Before
	public void beforeEach() {
		Mockito.when(modelMapper.map(Mockito.any(), Mockito.any())).thenReturn(Mockito.mock(Loan.class));
		Mockito.when(webClientMock.get()).thenReturn(uriSpecMock);
		Mockito.when(uriSpecMock.uri(ArgumentMatchers.any(Function.class))).thenReturn(headersSpecMock);
		Mockito.when(headersSpecMock.header(Mockito.notNull(), Mockito.notNull())).thenReturn(headersSpecMock);
		Mockito.when(headersSpecMock.headers(Mockito.notNull())).thenReturn(headersSpecMock);
		Mockito.when(headersSpecMock.accept(Mockito.notNull())).thenReturn(headersSpecMock);
		Mockito.when(headersSpecMock.exchange()).thenReturn(Mono.just(clientResponseMock));
		Mockito.when(clientResponseMock.headers()).thenReturn(responseHeadersMock);
	}

	@Test
	public void testLoadLoans_singlePageResult() {
		Supplier<LoanDTO> generator = () -> Mockito.mock(LoanDTO.class);
		Flux<LoanDTO> loansMock = Flux.fromStream(Stream.generate(generator).limit(5));

		Mockito.when(clientResponseMock.statusCode()).thenReturn(HttpStatus.OK);
		Mockito.when(clientResponseMock.bodyToFlux(LoanDTO.class)).thenReturn(loansMock);
		Mockito.when(responseHeadersMock.header("X-Total")).thenReturn(Collections.singletonList("5"));

		Flux<Loan> loans = apiClient.loadLoans(startTime, endTime);
		StepVerifier.create(loans).expectNextCount(5).expectComplete().verify();
	}

	@Test
	public void testLoadLoans_multiPageResult() {
		Supplier<LoanDTO> generator = () -> Mockito.mock(LoanDTO.class);
		Flux<LoanDTO> loansMock_1to10 = Flux.fromStream(Stream.generate(generator).limit(10));
		Flux<LoanDTO> loansMock_10to15 = Flux.fromStream(Stream.generate(generator).limit(5));
		Queue<Flux<LoanDTO>> loansMocks = new LinkedList<>(Arrays.asList(loansMock_1to10, loansMock_10to15));

		Mockito.when(clientResponseMock.statusCode()).thenReturn(HttpStatus.OK);
		Mockito.when(clientResponseMock.bodyToFlux(LoanDTO.class)).then(invocation -> loansMocks.poll());
		Mockito.when(responseHeadersMock.header("X-Total")).thenReturn(Collections.singletonList("15"));

		Flux<Loan> loans = apiClient.loadLoans(startTime, endTime);
		StepVerifier.create(loans).expectNextCount(15).expectComplete().verify();
	}

	@Test
	public void testLoadLoans_apiError() {
		Mockito.when(clientResponseMock.statusCode()).thenReturn(HttpStatus.INTERNAL_SERVER_ERROR);

		Flux<Loan> loans = apiClient.loadLoans(startTime, endTime);
		StepVerifier.create(loans).expectError().verify();
	}
}
