package chudanic.petr.zonky.scheduling;

import java.time.LocalDateTime;

import chudanic.petr.zonky.service.LoanLoggingService;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.mockito.Mockito;

public class LoadSchedulerTest {

    @Test
    public void scheduledLoansLoad_firstLoad() {
        LoanLoggingService loanLoggingService = Mockito.mock(LoanLoggingService.class);
        final int loggingIntervalInMillis = 300;
        LoansLoadScheduler scheduler = new LoansLoadScheduler(loggingIntervalInMillis, loanLoggingService);
		LocalDateTime initialLastLoad = scheduler.getLastLoanLoad();

        scheduler.scheduledLoansLoad();
        LocalDateTime lastLoanLoad = scheduler.getLastLoanLoad();

        Mockito.verify(loanLoggingService, Mockito.times(1)).loadAndLogLoans(initialLastLoad, lastLoanLoad);
        Mockito.verifyNoMoreInteractions(loanLoggingService);
		Assertions.assertThat(initialLastLoad).isBefore(lastLoanLoad);
    }

    @Test
    public void scheduledLoansLoad_twoLoads() {
        LoanLoggingService loanLoggingService = Mockito.mock(LoanLoggingService.class);
        final int loggingIntervalInMillis = 300;
        LoansLoadScheduler scheduler = new LoansLoadScheduler(loggingIntervalInMillis, loanLoggingService);
		LocalDateTime initialLastLoad = scheduler.getLastLoanLoad();

        scheduler.scheduledLoansLoad();
        LocalDateTime lastLoanLoad_1 = scheduler.getLastLoanLoad();
        scheduler.scheduledLoansLoad();
        LocalDateTime lastLoanLoad_2 = scheduler.getLastLoanLoad();

        Mockito.verify(loanLoggingService, Mockito.times(1)).loadAndLogLoans(initialLastLoad, lastLoanLoad_1);
        Mockito.verify(loanLoggingService, Mockito.times(1)).loadAndLogLoans(lastLoanLoad_1, lastLoanLoad_2);
        Mockito.verifyNoMoreInteractions(loanLoggingService);
		Assertions.assertThat(initialLastLoad).isBefore(lastLoanLoad_1);
		Assertions.assertThat(lastLoanLoad_1).isBeforeOrEqualTo(lastLoanLoad_2);
    }
}