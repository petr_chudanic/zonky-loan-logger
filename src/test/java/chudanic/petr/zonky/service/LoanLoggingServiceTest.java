package chudanic.petr.zonky.service;

import java.time.LocalDateTime;
import java.util.function.Supplier;
import java.util.stream.Stream;

import chudanic.petr.zonky.apiclient.ZonkyApiClient;
import chudanic.petr.zonky.logging.LoanLogWriter;
import chudanic.petr.zonky.model.Loan;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import reactor.core.publisher.Flux;

public class LoanLoggingServiceTest {

	private final LocalDateTime startTime = LocalDateTime.MIN;

	private final LocalDateTime endTime = LocalDateTime.MAX;

	@Test
	public void loadAndLogLoans_loansLogged() {
		ZonkyApiClient client = Mockito.mock(ZonkyApiClient.class);
		LoanLogWriter logger = Mockito.mock(LoanLogWriter.class);
		LoanLoggingService service = new LoanLoggingService(client, logger);
		Supplier<Loan> generator = () -> Mockito.mock(Loan.class);
		Flux<Loan> loansMock = Flux.fromStream(Stream.generate(generator).limit(1000));
		Mockito.when(client.loadLoans(startTime, endTime)).thenReturn(loansMock);

		service.loadAndLogLoans(startTime, endTime);

		Mockito.verify(client, Mockito.times(1)).loadLoans(startTime, endTime);
		Mockito.verify(logger, Mockito.times(1000)).logLoan(ArgumentMatchers.any(Loan.class));
		Mockito.verify(logger, Mockito.never()).logError(ArgumentMatchers.any());
	}

	@Test
	public void loadAndLogLoans_error() {
		ZonkyApiClient client = Mockito.mock(ZonkyApiClient.class);
		LoanLogWriter logger = Mockito.mock(LoanLogWriter.class);
		LoanLoggingService service = new LoanLoggingService(client, logger);
		Exception expectedException = new Exception("This is exception");
		Flux<Loan> error = Flux.error(expectedException);
		Mockito.when(client.loadLoans(startTime, endTime)).thenReturn(error);

		service.loadAndLogLoans(startTime, endTime);

		Mockito.verify(client, Mockito.times(1)).loadLoans(startTime, endTime);
		Mockito.verify(logger, Mockito.never()).logLoan(ArgumentMatchers.any(Loan.class));
		Mockito.verify(logger, Mockito.times(1)).logError(expectedException);
	}
}